import { CALL_API, Schemas } from "../middleware/api";

export const STAFF_REQUEST = "STAFF_REQUEST";
export const STAFF_SUCCESS = "STAFF_SUCCESS";
export const STAFF_FAILURE = "STAFF_FAILURE";

export const CHANGE_CHOSEN = "CHANGE_CHOSEN";

const fetchStaff = () => ({
  [CALL_API]: {
    types: [STAFF_REQUEST, STAFF_SUCCESS, STAFF_FAILURE],
    endpoint: "",
    schema: Schemas.STAFF,
  },
});

const changeStaff = key => ({
  type: CHANGE_CHOSEN,
  key,
});

export const CHOOSE_ALL = "CHOOSE_ALL";

const chooseAllStaff = choose => ({
  type: CHOOSE_ALL,
  choose,
});

export const loadStaff = () => (dispatch, getState) => {
  return dispatch(fetchStaff());
};

export const changeChosen = key => (dispatch, getState) => {
  return dispatch(changeStaff(key));
};

export const chooseAll = choose => (dispatch, getState) => {
  return dispatch(chooseAllStaff(choose));
};
