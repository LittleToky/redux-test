/* eslint-disable no-undef */
import { Table } from "antd";

import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { loadStaff, changeChosen, chooseAll } from "../actions";
import "antd/dist/antd.css";
import "./styles.css";

class App extends Component {
  static propTypes = {
    loadStaff: PropTypes.func.isRequired,
    changeChosen: PropTypes.func.isRequired,
    chooseAll: PropTypes.func.isRequired,
  };

  componentDidMount = () => {
    this.props.loadStaff(this.props);
  };

  columns = [
    {
      title: "Имя",
      dataIndex: "first_name",
      key: "first_name",
    },
    {
      title: "Фамилия",
      dataIndex: "last_name",
      key: "last_name",
    },
    {
      title: "Возраст",
      dataIndex: "age",
      key: "age",
    },
  ];

  rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      const oldKeys = Object.values(this.props.staff)
        .filter(person => person.chosen)
        .map(person => person.key);
      const oldL = oldKeys.length;
      const newL = selectedRowKeys.length;
      if (newL - oldL < -1) {
        this.props.chooseAll(false);
      } else if (newL - oldL > 1) {
        this.props.chooseAll(true);
      } else {
        const key =
          newL > oldL
            ? selectedRowKeys.filter(key => !oldKeys.includes(key))
            : oldKeys.filter(key => !selectedRowKeys.includes(key));
        this.props.changeChosen(key);
      }
    },
    getCheckboxProps: record => ({
      disabled: record.name === "Disabled User", // Column configuration not to be checked
    }),
  };

  render() {
    const { staff } = this.props;
    const chosen = Object.values(staff).filter(person => person.chosen);
    this.rowSelection.selectedRowKeys = chosen.map(person => person.key);
    return (
      <div className="wrapper">
        <Table
          className="ant-table"
          rowSelection={this.rowSelection}
          dataSource={Object.values(staff)}
          columns={this.columns}
          title={() => "ПЕРСОНАЛ"}
          loading={Object.keys(staff).length === 0}
        />
        <div className="names">Пользователи:</div>
        <div className="names">
          {chosen.map(person => (
            <div key={person.key}>
              {person.first_name} {person.last_name}
            </div>
          ))}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  staff: state.staff,
});

export default withRouter(
  connect(
    mapStateToProps,
    {
      loadStaff,
      changeChosen,
      chooseAll,
    }
  )(App)
);
