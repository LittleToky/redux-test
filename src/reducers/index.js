import { combineReducers } from "redux";

const staff = (state = {}, action) => {
  if (action.response && action.response.entities) {
    return action.response.entities.staff;
  }

  let newState = { ...state };
  switch (action.type) {
    case "CHANGE_CHOSEN":
      const key = action.key.toString();
      const chosen = newState[key].chosen;
      newState[key].chosen = !chosen;
      return newState;
    case "CHOOSE_ALL":
      const { choose } = action;
      Object.entries(newState).forEach(person => {
        person[1].chosen = choose;
        newState[person[0]] = person[1];
      });
      return { ...newState };
    default:
      return state;
  }
};

const rootReducer = combineReducers({
  staff,
});

export default rootReducer;
